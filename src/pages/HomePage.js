import React from "react";
import CarouselComponent from "../components/Carousel/CarouselComponent";

function HomePage() {
  return (
    <div>
      <CarouselComponent />
    </div>
  );
}

export default HomePage;
