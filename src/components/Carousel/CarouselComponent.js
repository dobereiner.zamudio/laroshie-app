import React, { useState } from "react";
import CarouselItem from "./CarouselItem";
import Carousel from "react-material-ui-carousel";
import Crop169Icon from "@mui/icons-material/Crop169";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import CarouselItem1 from "../../assets/carouselItem1.jpg";
import CarouselItem2 from "../../assets/carouselItem2.jpg";
import CarouselItem3 from "../../assets/carouselItem3.jpg";
import CarouselItem4 from "../../assets/carouselItem4.jpg";

function CarouselComponent() {
  const [firstLoaded, setFirstLoaded] = useState(false);

  // Callback to check if carousel item is loaded for the first time
  // Fix to carousel image not growing to full height when loaded first time
  const firstLoadedCallback = (index) => {
    setFirstLoaded(true);
  };

  return (
    <div>
      <Carousel
        animation="slide"
        swipe="true"
        IndicatorIcon={<Crop169Icon />}
        NextIcon={<KeyboardArrowRightIcon />}
        PrevIconIcon={<KeyboardArrowLeftIcon />}
        activeIndicatorIconButtonProps={{
          style: {
            color: "#f3ff17",
          },
        }}
        indicatorContainerProps={{
          style: {
            zIndex: 1,
            marginTop: "-30px",
            position: "relative",
          },
        }}
      >
        {
          <CarouselItem
            key={1}
            item={CarouselItem1}
            firstLoaded={firstLoadedCallback}
          />
        }
        {firstLoaded && (
          <CarouselItem
            key={2}
            item={CarouselItem2}
            firstLoaded={firstLoadedCallback}
          />
        )}
        {firstLoaded && (
          <CarouselItem
            key={3}
            item={CarouselItem3}
            firstLoaded={firstLoadedCallback}
          />
        )}
        {firstLoaded && (
          <CarouselItem
            key={4}
            item={CarouselItem4}
            firstLoaded={firstLoadedCallback}
          />
        )}
      </Carousel>
    </div>
  );
}

export default CarouselComponent;
