import React from "react";
import { Paper } from "@mui/material";
import "./../Carousel/CarouselItem.css";

function CarouselItem({ item, firstLoaded }) {
  return (
    <div>
      <Paper>
        <div className="carousel-item-container">
          <img
            onLoad={() => {
              firstLoaded(true);
            }}
            src={item}
            alt={item}
            style={{
              width: "100%",
              objectFit: "cover",
              height: "100%",
            }}
          />
        </div>
      </Paper>
    </div>
  );
}

export default CarouselItem;
