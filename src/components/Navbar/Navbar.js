import React, { useState } from "react";
import { Link } from "react-router-dom";
import "../Navbar/Navbar.css";
import Logo from "../../assets/logo.png";
import ReorderIcon from "@mui/icons-material/Reorder";

function Navbar() {
  const [openLinks, setOpenLinks] = useState(false);

  const onButtonClick = () => {
    setOpenLinks(!openLinks);
  };

  //Handle hiddenLinks when window is resized to <= 600 width
  React.useEffect(() => {
    function handleResize() {
      setOpenLinks(false);
    }
    window.addEventListener("resize", handleResize);
  }, []);

  return (
    <div className="navbar-container">
      <div className="leftSide" id={openLinks ? "open" : "close"}>
        <img src={Logo} alt="logo" />
        <h1>Laroshie</h1>
        <div className="hiddenLinks">
          <Link to="/"> Home</Link>
          <Link to="/"> Games</Link>
          <Link to="/"> About</Link>
          <Link to="/"> Contact</Link>
        </div>
      </div>
      <div className="rightSide">
        <Link to="/"> Home</Link>
        <Link to="/"> Games</Link>
        <Link to="/"> About</Link>
        <Link to="/"> Contact</Link>
        <button onClick={onButtonClick}>
          <ReorderIcon />
        </button>
      </div>
    </div>
  );
}

export default Navbar;
